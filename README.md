# Gamesys coding test

## Registration with Exclusion Services

### Introduction

This is a microservice with a rest endpoint with following definition:

POST method at /register, receive a json in body with user to register.

Check:

* if all parameters (username, password, dateOfBirth, ssn) are filled.
* if username and password are valid. 
* if the ssn is not blacklisted.
* if the username is not already registered.

Returns:

* http status 200 without response body when user is successfully inserted.
* http status 400 with a json with detail message when validation error occurs.
* http status 500 when a unexpected error occurs.

For more details, you can run the application and access swagger documentation at /swagger-ui.html .
	
### Assumptions

In this project I`m assuming some undocumented rules:

* User is considered as registered by username, in other words, cannot have 2 users with same username.
* The username, password, date of birth and SSN are required.
* Can have more than one user with same SSN and date of birth.
* SSN format is really not validated, can send "abc123$%$(*@" as a SSN, but it can be validated with following regular expression: _^\d{3}-\d{2}-\d{4}$_

### Tecnology

This project is using these following technologies/frameworks:

Base:

* [Spring boot](https://spring.io/projects/spring-boot) framework.
* [Undertow](http://undertow.io/) for web server.

Validation: 

* [Bean validation](https://beanvalidation.org/) for data validation.
* [Spring validator](https://docs.spring.io/spring/docs/4.1.x/spring-framework-reference/html/validation.html) for call bean validation.

Api documentation:

* [Swagger](https://swagger.io/) for rest api documentation.

Testing:

* [JUnit](https://junit.org/junit4/) as test base execution framework.
* [Spring test](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html) for test with spring framework.
* [Assertj](http://joel-costigliola.github.io/assertj/) for assertion framework.
* [Mockito](https://site.mockito.org/) for mocking.
* [Cucumber](https://cucumber.io/) for BDD scenarios.

Logging:

* [Slf4j](https://www.slf4j.org/) as logging interface.
* [Logback](https://logback.qos.ch/) as log provider for slf4j interface.

Code generation:

* [Lombok](https://projectlombok.org/) as a tool to generate automatically getters, setters, equals, hashcode, constructors and builders by annotations, need to configure the IDE to work properly, if you have any problem with lombok, there is a branch **without-lombok** with equivalent implementation without lombok.

### Running

#### Requirements 

* Java 1.8 
* Maven
* Docker (if you want to run on docker container engine)

#### Running

To run this app you can run by docker, maven or java executable running following commands at root folder of project:

maven: ``mvn spring-boot:run``

java:

* building jar: ``mvn clean -U package``
* running jar: ``java -jar ./target/gamesys-registration-1.0.0.jar``


docker:

* building image: ``mvn clean -U package docker:build``
* running: docker: ``docker run --name exercise-registration gamesys-registration:1.0.0`` 


### Roadmap

There are some improvements to do on this project:

* Separate _com.gamesys.exercise.common_ and _com.gamesys.exercise.exclusion_ packages in isolated java libraries.
* Implement Exclusion service properly
* Implement a persistence in database 