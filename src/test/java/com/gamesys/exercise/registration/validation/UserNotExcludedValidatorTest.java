package com.gamesys.exercise.registration.validation;

import static com.gamesys.exercise.registration.RegistrationTestBase.getSampleBlacklistedUser;
import static com.gamesys.exercise.registration.RegistrationTestBase.getSampleNewUser;
import static com.gamesys.exercise.registration.RegistrationTestBase.initMockExclusionService;
import static com.gamesys.exercise.registration.validation.ValidationTestBase.assertValidationFailWithMessage;
import static com.gamesys.exercise.registration.validation.ValidationTestBase.assertValidationSuccess;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.gamesys.exercise.common.validation.ValidationResult;
import com.gamesys.exercise.exclusion.service.ExclusionService;
import com.gamesys.exercise.registration.domain.User;

public class UserNotExcludedValidatorTest {

	
	private UserNotExcludedValidator validator;
	
	@Before
	public void setup() {
		ExclusionService exclusionServiceMock = Mockito.mock(ExclusionService.class);
		initMockExclusionService(exclusionServiceMock);
		validator = new UserNotExcludedValidator(exclusionServiceMock);
	}

	@Test
	public void given_a_null_user_when_validate_then_successfully_validate() {
		User user = null;

		ValidationResult result = validator.apply(user);
		
		assertValidationSuccess(result);
	}

	@Test
	public void given_a_user_with_a_null_ssn_when_validate_then_successfully_validate() {
		User user = getSampleNewUser();
		user.setSsn(null);

		ValidationResult result = validator.apply(user);
		
		assertValidationSuccess(result);
	}
	
	@Test
	public void given_a_user_with_a_null_date_of_birth_when_validate_then_successfully_validate() {
		User user = getSampleNewUser();
		user.setDateOfBirth(null);

		ValidationResult result = validator.apply(user);
		
		assertValidationSuccess(result);
	}
	
	@Test
	public void given_a_user_with_a_blacklisted_ssn_and_date_of_birth_when_validate_then_validation_fail() {
		User user = getSampleBlacklistedUser();

		ValidationResult result = validator.apply(user);
		
		assertValidationFailWithMessage(result, "User SSN is blacklisted.");
	}
}
