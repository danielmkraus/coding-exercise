package com.gamesys.exercise.registration.validation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.gamesys.exercise.exclusion.service.ExclusionService;
import com.gamesys.exercise.registration.RegistrationTestBase;
import com.gamesys.exercise.registration.domain.User;
import com.gamesys.exercise.registration.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserValidationTest extends RegistrationTestBase {

	@Autowired
	UserValidator userValidation;

	@MockBean
	ExclusionService exclusionServiceMock;

	@MockBean
	UserRepository userRepositoryMock;

	private User user;

	@Before
	public void setup() {
		initValidUser();
		initMockExclusionService(exclusionServiceMock);
		initMockUserRepository(userRepositoryMock);
	}

	private void initValidUser() {
		user = getSampleNewUser();
	}

	@Test
	public void given_a_valid_user_when_validate_then_is_valid() {
		userValidation.validate(user);
	}

	@Test(expected = IllegalArgumentException.class)
	public void given_a_blacklisted_user_when_validate_then_is_not_valid() {
		userValidation.validate(getSampleBlacklistedUser());
	}

	@Test(expected = IllegalArgumentException.class)
	public void given_a_null_user_when_validate_then_is_not_valid() {
		user = null;
		userValidation.validate(user);
	}

	@Test(expected = IllegalArgumentException.class)
	public void given_a_null_username_when_validate_then_is_not_valid() {
		user.setUsername(null);
		userValidation.validate(user);
	}

	@Test(expected = IllegalArgumentException.class)
	public void given_a_already_registered_username_when_validate_then_is_not_valid() {
		user.setUsername(SAMPLE_REGISTERED_USERNAME);
		userValidation.validate(user);
	}
}
