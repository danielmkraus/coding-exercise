package com.gamesys.exercise.registration.validation;

import static com.gamesys.exercise.registration.RegistrationTestBase.getSampleNewUser;
import static com.gamesys.exercise.registration.validation.ValidationTestBase.assertValidationFailWithMessage;
import static com.gamesys.exercise.registration.validation.ValidationTestBase.assertValidationSuccess;

import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.gamesys.exercise.common.validation.ValidationResult;
import com.gamesys.exercise.registration.domain.User;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserBeanValidatorTest {

	@Autowired
	UserBeanValidator beanValidator;
	
	@Test(expected=IllegalArgumentException.class)
	public void given_a_null_user_when_validate_then_throws_illegalargumentexception() {
		User user = null; 
		
		beanValidator.apply(user);
	}

	@Test
	public void given_a_valid_user_when_validate_then_successfully_validate() {
		User user = getSampleNewUser(); 
		
		ValidationResult result = beanValidator.apply(user);
		
		assertValidationSuccess(result);
	}
	
	@Test
	public void given_a_user_without_a_required_field_when_validate_then_fail_validation() {
		User sampleNewUser = getSampleNewUser();
		sampleNewUser.setDateOfBirth(null);
		validateFail(sampleNewUser, "Date of birth is required.");
		
		sampleNewUser = getSampleNewUser();
		sampleNewUser.setPassword(null);
		validateFail(sampleNewUser, "Password is required.");

		sampleNewUser = getSampleNewUser();
		sampleNewUser.setSsn(null);
		validateFail(sampleNewUser, "SSN is required.");
		
		sampleNewUser = getSampleNewUser();
		sampleNewUser.setUsername(null);
		validateFail(sampleNewUser, "Username is required.");
	}
	
	@Test
	public void given_a_user_with_a_invalid_username_when_validate_then_fail_validation() {
		Stream.of(
				"",
				" ",
				"a a",
				"a_",
				"s@",
				"A$")
		.map(invalidUsername->{
			User user = getSampleNewUser(); 
			user.setUsername(invalidUsername); 
			return user;}
		).forEach(user-> validateFail(user, "Username is invalid."));
	}

	@Test
	public void given_a_user_with_a_invalid_password_when_validate_then_fail_validation() {
		Stream.of(
				"",
				" ",
				"a a",
				"a_",
				"s@",
				"A$",
				"AAaa",
				"aaa1")
		.map(invalidPassword->{
			User user = getSampleNewUser(); 
			user.setPassword(invalidPassword); 
			return user;}
		).forEach(user-> validateFail(user, "Password is invalid."));
	}
	
	private <T> void validateFail(User user, String validationMessage) {
		ValidationResult result = beanValidator.apply(user);
		assertValidationFailWithMessage(result, validationMessage);
	}
}
