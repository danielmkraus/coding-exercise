package com.gamesys.exercise.registration.validation;

import static com.gamesys.exercise.registration.RegistrationTestBase.SAMPLE_REGISTERED_USERNAME;
import static com.gamesys.exercise.registration.RegistrationTestBase.SAMPLE_VALID_USERNAME;
import static com.gamesys.exercise.registration.RegistrationTestBase.getSampleRegisteredUser;
import static com.gamesys.exercise.registration.RegistrationTestBase.initMockUserRepository;
import static com.gamesys.exercise.registration.validation.ValidationTestBase.assertValidationFailWithMessage;
import static com.gamesys.exercise.registration.validation.ValidationTestBase.assertValidationSuccess;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;

import com.gamesys.exercise.common.validation.ValidationResult;
import com.gamesys.exercise.registration.domain.User;
import com.gamesys.exercise.registration.repository.UserRepository;


public class UsernameUniqueValidatorUnitTest {

	
	private UsernameUniqueValidator validator;
	
	private UserRepository userRepositoryMock;
	
	@Before
	public void setup() {
		userRepositoryMock = mock(UserRepository.class);
		initMockUserRepository(userRepositoryMock);
		validator = new UsernameUniqueValidator(userRepositoryMock);
	}

	
	@Test
	public void given_a_null_user_when_validate_then_successfully_validate() {
		User user = null;
		
		ValidationResult result = validator.apply(user);
		
		assertValidationSuccess(result);
	}
	
	@Test
	public void given_a_null_username_when_validate_then_successfully_validate() {
		User user = User.builder().build();
		
		ValidationResult result = validator.apply(user);
		
		assertValidationSuccess(result);
	}

	@Test
	public void given_a_already_registered_username_with_same_id_when_validate_then_successfully_validate() {
		User user = getSampleRegisteredUser();
		
		ValidationResult result = validator.apply(user);
		
		assertValidationSuccess(result);
	}

	@Test
	public void given_a_not_registered_username_when_validate_then_successfully_validate() {
		User user = User.builder().username(SAMPLE_VALID_USERNAME).build();
		assertFalse(userRepositoryMock.findByUsername(user.getUsername()).isPresent());
		
		ValidationResult result = validator.apply(user);
		
		assertValidationSuccess(result);
	}
	
	@Test
	public void given_a_already_registered_username_when_validate_then_fail_validation() {
		User user = User.builder().username(SAMPLE_REGISTERED_USERNAME).build();
		assertTrue(userRepositoryMock.findByUsername(user.getUsername()).isPresent());

		ValidationResult result = validator.apply(user);
		
		assertValidationFailWithMessage(result, "Username already registered.");
	}
}
