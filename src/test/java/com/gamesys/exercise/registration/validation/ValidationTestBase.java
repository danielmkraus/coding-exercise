package com.gamesys.exercise.registration.validation;

import static org.assertj.core.api.Assertions.*;

import com.gamesys.exercise.common.validation.ValidationResult;

public class ValidationTestBase {

	public static void assertValidationSuccess(ValidationResult result) {
		assertThat(result).isNotNull();
		assertThat(result.isValid()).isTrue();
		assertThat(result.getReason()).isEmpty();
	}
	

	public static void assertValidationFailWithMessage(ValidationResult result, String message) {
		assertThat(result).isNotNull();
		assertThat(result.isValid()).isFalse();
		assertThat(result.getReason()).isNotEmpty().hasValue(message);
	}
}
