package com.gamesys.exercise.registration.converter;

import static org.junit.Assert.assertNull;

import java.time.LocalDate;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.gamesys.exercise.registration.RegistrationTestBase;
import com.gamesys.exercise.registration.domain.User;
import com.gamesys.exercise.registration.dto.RegisterUser;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RegisterUserToUserConverterTest extends RegistrationTestBase {

	/**
	 * common property names between {@link User} and {@link RegisterUser}
	 */
	private static final String[] COMMON_PROPERTIES = {"password", "username", "dateOfBirth", "ssn"};
	
	
	@Autowired
	RegisterUserToUserConverter converter;
	
	@Test(expected=NullPointerException.class)
	public void given_null_when_convert_then_get_null_pointer_exception() {
		assertNull(converter.convert(null));
	}
	
	@Test
	public void given_null_properties_when_convert_then_get_null_properties() {
		RegisterUser source = new RegisterUser();
		
		User converted = converter.convert(source);
		Assertions.assertThat(converted).isNotNull().isEqualToComparingOnlyGivenFields(source,  COMMON_PROPERTIES);
	}
	
	@Test
	public void given_all_properties_filled_when_convert_then_get_all_properties_filled() {
		RegisterUser source = new RegisterUser();
		source.setDateOfBirth(LocalDate.now());
		source.setPassword(RegistrationTestBase.SAMPLE_REGISTERED_PASSWORD);
		source.setUsername(RegistrationTestBase.SAMPLE_REGISTERED_USERNAME);
		source.setSsn(RegistrationTestBase.SAMPLE_REGISTERED_SSN);
		
		User converted = converter.convert(source);
		Assertions.assertThat(converted).isNotNull().isEqualToComparingOnlyGivenFields(source,  COMMON_PROPERTIES);
	}
}
