package com.gamesys.exercise.registration.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collection;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.gamesys.exercise.common.repository.Wipeable;
import com.gamesys.exercise.registration.RegistrationTestBase;
import com.gamesys.exercise.registration.domain.User;
import com.gamesys.exercise.registration.repository.UserRepository;

/**
 * service test (without mocked parts) of {@link UserService}
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest extends RegistrationTestBase {

	@Autowired
	private UserService userService;
	
	@Autowired
	private UserRepository repository;
	
	@Autowired
	Collection<Wipeable> wipeables;
	
	@Before
	public void setup() {
		repository.save(getSampleRegisteredUser());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void given_null_when_register_then_fail() {
		User user = null;
		
		userService.register(user);
	}
	
	@Test
	public void given_new_user_when_register_then_sucessful_register() {
		User user = getSampleNewUser();
		
		userService.register(user);
		
		Optional<User> savedUser = repository.findByUsername(user.getUsername());
		assertThat(savedUser.isPresent()).isTrue();
		assertThat(savedUser.get()).isEqualToComparingFieldByField(user);
	}
}
