package com.gamesys.exercise.registration.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.gamesys.exercise.registration.RegistrationTestBase;
import com.gamesys.exercise.registration.domain.User;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest extends RegistrationTestBase{

	
	@Autowired
	UserRepository userRepository;
	
	@Before
	public void setup() {
		userRepository.save(getSampleRegisteredUser());
	}
	
	

	@Test
	public void given_a_new_user_when_save_then_save_new_user() {
		User user = new User();
		user.setUsername(SAMPLE_VALID_USERNAME);
		
		userRepository.save(user);
		
		assertNotNull(user.getId());
		assertThat(userRepository.findById(user.getId())).isNotNull().isEqualTo(user);
	}
	
	@Test
	public void given_a_not_registered_id_when_find_user_by_id_then_return_null() {
		assertNull(userRepository.findById(UUID.randomUUID().toString()));
	}
	
	@Test
	public void given_a_not_registered_username_when_find_user_by_username_then_return_null() {
		assertThat(userRepository.findByUsername(SAMPLE_VALID_USERNAME).isPresent()).isFalse();
	}
	
	@Test
	public void given_a_registered_id_when_find_user_by_id_then_return_user() {
		User user = userRepository.findById(SAMPLE_REGISTERED_ID);
		
		assertThat(user).satisfies(this::isAlreadyRegisteredUser);
	}

	@Test
	public void given_a_registered_username_when_find_user_by_username_then_return_user() {
		Optional<User> user = userRepository.findByUsername(SAMPLE_REGISTERED_USERNAME);	
		assertThat(user.isPresent()).isTrue();
		assertThat(user.get()).satisfies(this::isAlreadyRegisteredUser);
	}
	
	private void isAlreadyRegisteredUser(User user) {
		assertThat(user).isNotNull();
		assertThat(user.getId()).isEqualTo(SAMPLE_REGISTERED_ID);
		assertThat(user.getUsername()).isEqualTo(SAMPLE_REGISTERED_USERNAME);
	}
}
