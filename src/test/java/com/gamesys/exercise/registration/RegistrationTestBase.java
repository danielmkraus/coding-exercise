package com.gamesys.exercise.registration;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Optional;

import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;

import com.gamesys.exercise.common.repository.Wipeable;
import com.gamesys.exercise.exclusion.service.ExclusionService;
import com.gamesys.exercise.registration.domain.User;
import com.gamesys.exercise.registration.repository.UserRepository;

public class RegistrationTestBase {

	@Autowired(required=false)
	Collection<Wipeable> wipeables;

	@After
	public void tearDown() {
		Optional.ofNullable(wipeables).ifPresent(list-> list.parallelStream().forEach(Wipeable::wipe));
	}
	

	public static final String BLACKLISTED_SSN = "012-34-5678";
	public static final String BLACKLISTED_DATE_OF_BIRTH = "1990-01-01";
	
	public static final String SAMPLE_VALID_USERNAME = "foo";
	public static final String SAMPLE_VALID_SSN = "000-01-0002";
	public static final String SAMPLE_VALID_PASSWORD = "Secr3t";

	public static final String SAMPLE_REGISTERED_ID = "1";
	public static final String SAMPLE_REGISTERED_USERNAME = "bar";
	public static final String SAMPLE_REGISTERED_SSN = "000-01-0003";
	public static final String SAMPLE_REGISTERED_PASSWORD = "Secr3t2";
	
	public static User getSampleBlacklistedUser() {
		return User.builder()
				.dateOfBirth(LocalDate.parse(BLACKLISTED_DATE_OF_BIRTH, DateTimeFormatter.ISO_LOCAL_DATE))
				.ssn(BLACKLISTED_SSN)
				.username(SAMPLE_VALID_USERNAME)
				.password(SAMPLE_VALID_PASSWORD)
					.build();	
	}
	
	public static User getSampleNewUser() {
		return User.builder()
				.dateOfBirth(LocalDate.now())
				.username(SAMPLE_VALID_USERNAME)
				.password(SAMPLE_VALID_PASSWORD)
				.ssn(SAMPLE_VALID_SSN)
					.build();
	}
	
	public static User getSampleRegisteredUser() {
		return User.builder()
				.dateOfBirth(LocalDate.now())
				.username(SAMPLE_REGISTERED_USERNAME)
				.password(SAMPLE_REGISTERED_PASSWORD)
				.ssn(SAMPLE_REGISTERED_SSN)
				.id(SAMPLE_REGISTERED_ID)
					.build();
	}
	
	public static void initMockExclusionService(ExclusionService exclusionServiceMock) {
		when(exclusionServiceMock.validate(anyString(), anyString()))
				.thenReturn(true);
		when(exclusionServiceMock.validate(BLACKLISTED_DATE_OF_BIRTH, BLACKLISTED_SSN))
				.thenReturn(false);
	}

	public static void initMockUserRepository(UserRepository userRepositoryMock) {
		when(userRepositoryMock.findByUsername(anyString()))
				.thenReturn(Optional.empty());
		when(userRepositoryMock.findByUsername(SAMPLE_REGISTERED_USERNAME))
				.thenReturn(Optional.of(getSampleRegisteredUser()));
	}
}
