package com.gamesys.exercise.integration.feature;

import static org.assertj.core.api.Assertions.assertThat;

import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import com.gamesys.exercise.common.exception.RestResponseEntityExceptionHandler.ErrorMessage;
import com.gamesys.exercise.exclusion.service.ExclusionFakeService;
import com.gamesys.exercise.integration.SpringIntegrationTest;
import com.gamesys.exercise.registration.domain.User;
import com.gamesys.exercise.registration.dto.RegisterUser;
import com.gamesys.exercise.registration.repository.UserRepository;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class RegisterStepsDefinition extends SpringIntegrationTest {

	private static final String PROPERTY_DATE_OF_BIRTH = "dateOfBirth";
	private static final String PROPERTY_PASSWORD = "password";
	private static final String PROPERTY_SSN = "ssn";
	private static final String PROPERTY_USERNAME = "username";

	private static final String ENDPOINT_REGISTER = "/register";

	@Autowired
	ExclusionFakeService exclusionService;

	@Autowired
	TestRestTemplate template;

	@Autowired
	UserRepository repository;

	private RegisterUser newUser;

	private ResponseEntity<ErrorMessage> response;

	@After
	@Override
	public void tearDown() {
		super.tearDown();
	}
	

	@Given("^a registered user with:$")
	public void a_registered_user_with(List<Map<String, String>> users) throws Throwable {
		users.forEach(this::saveNewUser);
	}

	private void saveNewUser(Map<String, String> properties) {
		ResponseEntity<ErrorMessage> response = template
				.exchange(RequestEntity.post(URI.create(ENDPOINT_REGISTER)).body(properties), ErrorMessage.class);
		assertThat(response).isNotNull();
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

	private ResponseEntity<ErrorMessage> saveNewUser(RegisterUser user) {
		return template.exchange(RequestEntity.post(URI.create(ENDPOINT_REGISTER)).body(user), ErrorMessage.class);
	}

	@Given("^this SSN is blacklisted$")
	public void this_SSN_is_blacklisted(List<Map<String, String>> blacklistedSSNs) throws Throwable {
		these_SSNs_are_blacklisted(blacklistedSSNs);
	}
	
	@Given("^these SSNs are blacklisted$")
	public void these_SSNs_are_blacklisted(List<Map<String, String>> blacklistedSSNs) throws Throwable {
		blacklistedSSNs
				.forEach(ssn -> assertThat(exclusionService.validate(ssn.get(PROPERTY_DATE_OF_BIRTH), ssn.get("ssn")))
						.isFalse());
	}

	@Given("^a new user to register with:$")
	public void a_new_user_to_register_with(List<Map<String, String>> propertiesList) throws Throwable {

		Map<String, String> properties = propertiesList.get(0);
		newUser = new RegisterUser();
		newUser.setDateOfBirth(
				LocalDate.parse(properties.get(PROPERTY_DATE_OF_BIRTH), DateTimeFormatter.ISO_LOCAL_DATE));
		newUser.setPassword(properties.get(PROPERTY_PASSWORD));
		newUser.setUsername(properties.get(PROPERTY_USERNAME));
		newUser.setSsn(properties.get(PROPERTY_SSN));

	}

	@Given("^new user to register with SSN \"([^\"]*)\" and date of birth \"([^\"]*)\"$")
	public void new_user_to_register_with_SSN_and_date_of_birth(String ssn, String dateOfBirth) throws Throwable {
		newUser.setDateOfBirth(LocalDate.parse(dateOfBirth, DateTimeFormatter.ISO_LOCAL_DATE));
		newUser.setSsn(ssn);
	}

	@Given("^exists a user registered with SSN \"([^\"]*)\" and date of birth \"([^\"]*)\"$")
	public void exists_a_user_registered_with_SSN_and_date_of_birth(String ssn, String dateOfBirth) throws Throwable {
		assertThat(repository.findBySsnAndDateOfBirth(ssn, LocalDate.parse(dateOfBirth, DateTimeFormatter.ISO_LOCAL_DATE))).isNotEmpty();
	}

	@Given("^new user to register with username \"([^\"]*)\"$")
	public void new_user_to_register_with_username(String username) throws Throwable {
		newUser.setUsername(username);
	}

	@Given("^exists a user with username \"([^\"]*)\"$")
	public void exists_a_user_with_username(String username) throws Throwable {
		Optional<User> user = repository.findByUsername(username);
		assertThat(user.isPresent()).isTrue();
	}

	@Given("^new user to register with password \"([^\\\"]*)\"$")
	public void new_user_to_register_with_password(String password) throws Throwable {
		newUser.setPassword(password);
	}

	@When("^register this user$")
	public void register_this_user() throws Throwable {
		response = saveNewUser(newUser);
	}

	@Then("^user is successfully registered$")
	public void user_is_successfully_registered() throws Throwable {
		assertThat(response).isNotNull();
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

	@Then("^will fail with message \"([^\"]*)\"$")
	public void will_fail_with_message(String message) throws Throwable {
		assertThat(response).isNotNull();
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
		assertThat(response.getBody().getMessage()).isEqualTo(message);
	}

}
