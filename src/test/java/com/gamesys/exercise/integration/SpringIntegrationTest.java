package com.gamesys.exercise.integration;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ContextConfiguration;

import com.gamesys.exercise.GamesysExcerciseApplication;
import com.gamesys.exercise.common.repository.Wipeable;

@ContextConfiguration(classes = GamesysExcerciseApplication.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT) 
public class SpringIntegrationTest {

	

	@Autowired(required=false)
	Collection<Wipeable> wipeables;

	public void tearDown() {
		Optional.ofNullable(wipeables).ifPresent(list-> list.parallelStream().forEach(Wipeable::wipe));
	}
}