@User @Registration
Feature: User registration
  As a user
  I want to register
  for use the system features

  Background: create a new user
    Given a registered user with:
      | username | password | ssn         | dateOfBirth |
      | user     | Secr3t   | 012-34-5678 | 2018-01-01  |
    And a new user to register with:
      | username | password | ssn         | dateOfBirth |
      | newuser  | Secr3t   | 012-34-5679 | 2018-01-01  |

  @Success
  Scenario: Successfully register a new user
    When register this user
    Then user is successfully registered

  @Success @SSN
  Scenario: Successfully register a new user with a already registered SSN and date of birth
    Given new user to register with SSN "012-34-5678" and date of birth "2018-01-01"
    And exists a user registered with SSN "012-34-5678" and date of birth "2018-01-01"
    When register this user
    Then user is successfully registered

  @Success @SSN
  Scenario: Success register a user with a SSN blacklisted but with distinct date of birth, it is rare,
    but SSN duplications can occour, so date of birth can diferenciate SSNs

    Given new user to register with SSN "000-00-0000" and date of birth "2018-01-01"
    And this SSN is blacklisted
      | ssn         | dateOfBirth |
      | 000-00-0000 | 2016-01-01  |
    When register this user
    Then user is successfully registered

  @Fail @SSN
  Scenario: Fail when register a user with blacklisted SSN
    Given new user to register with SSN "000-00-0000" and date of birth "2016-01-01"
    And this SSN is blacklisted
      | ssn         | dateOfBirth |
      | 000-00-0000 | 2016-01-01  |
    When register this user
    Then will fail with message "User SSN is blacklisted."

  @Fail @Password
  Scenario Outline: Fail to register a new user with a invalid password, the password need
    		 at least four characters, at least one upper case character, at least one number

    Given new user to register with password "<password>"
    When register this user
    Then will fail with message "Password is invalid."

    Examples: 
      | password | description                                                        |
      | abc1     | with 4 characters, with 1 digit, but without upper case character  |
      | aA1      | with 1 digit, with upper case character, but without 4 characters  |
      | aaaA     | with 4 characters, with 1 upper case character, but without digits |

  @Fail @Username
  Scenario: Fail when register a user with a already registered username
    Given new user to register with username "user"
    And exists a user with username "user"
    When register this user
    Then will fail with message "Username already registered."

  @Fail @Username
  Scenario Outline: Fail to register a new user with a invalid username, username is alphanumerical, no spaces
    Given new user to register with username "<username>"
    When register this user
    Then will fail with message "Username is invalid."

    Examples: 
      | username | description            |
      | abc$     | with special character |
      | ab_c     | with special character |
      | ab-c     | with special character |
      | abc.     | with special character |
      | aa aa    | with space             |
