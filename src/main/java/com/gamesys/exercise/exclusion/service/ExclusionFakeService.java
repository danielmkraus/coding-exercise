package com.gamesys.exercise.exclusion.service;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

import lombok.Data;
import lombok.NonNull;

@Service
public class ExclusionFakeService implements ExclusionService {

	
	private Set<Exclusion> exclusions;
	
	public ExclusionFakeService() {
		initFakeExclusions();
	}

	private void initFakeExclusions() {
		exclusions = Collections.synchronizedSet(new HashSet<>());
		exclusions.add(Exclusion.of("2016-01-01", "000-00-0000"));
		exclusions.add(Exclusion.of("2017-01-01", "000-00-0001"));
		exclusions.add(Exclusion.of("2018-01-01", "000-00-0002"));
	}
	 
	@Override
	public boolean validate(@NonNull String dateOfBirth, @NonNull String ssn) {
		return ! exclusions.contains(Exclusion.of(dateOfBirth, ssn));
	}

	@Data(staticConstructor="of")
	private static class Exclusion {
		@NonNull private String dateOfBirth;
		@NonNull private String ssn;
	}
}
