package com.gamesys.exercise.common.validation;

import java.util.ResourceBundle;
import java.util.function.Function;

/**
 * functional interface to do chained validation on object using combinator pattern
 *
 * https://gtrefs.github.io/code/combinator-pattern/
 * 
 * @see ValidationResult
 */
public interface Validation<X> extends Function<X, ValidationResult>{

	ResourceBundle VALIDATION_MESSAGES = ResourceBundle.getBundle("ValidationMessages");

    default Validation<X> and(Validation<X> other) {
        return user -> {
            final ValidationResult result = this.apply(user);
            return result.isValid() ? other.apply(user) : result;
        };
    }
}
