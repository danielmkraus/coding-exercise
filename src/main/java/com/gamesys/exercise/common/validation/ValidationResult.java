package com.gamesys.exercise.common.validation;

import java.util.Optional;

import lombok.Data;

/**
 * Result of validation applied in {@link Validation}
 *
 */
public interface ValidationResult{
    static ValidationResult valid(){
        return ValidationSupport.valid();
    }
    
    static ValidationResult invalid(String reason){
        return new Invalid(reason);
    }
    
    boolean isValid();
    
    default boolean isNotValid() {
    	return ! isValid();
    }
    
    Optional<String> getReason();
}

@Data
final class Invalid implements ValidationResult {
	
    private final String reason;

	public Optional<String> getReason(){ 
    	return Optional.of(reason); 
    }
    
    public boolean isValid(){
        return false;
    }
}

final class ValidationSupport {
    
	private static final ValidationResult valid = new ValidationResult(){
        
		public boolean isValid(){ 
        	return true; 
        }
        
		public Optional<String> getReason(){ 
        	return Optional.empty(); 
        }
    };
    
    static ValidationResult valid(){
        return valid;
    }
}
