package com.gamesys.exercise.common.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@ControllerAdvice
public class RestResponseEntityExceptionHandler 
  extends ResponseEntityExceptionHandler {
 
	private static final Logger LOGGER = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);
	
    @ExceptionHandler(IllegalArgumentException.class)
    protected ResponseEntity<Object> handleConstraintViolation(IllegalArgumentException e, WebRequest request) {
    	LOGGER.error(e.getMessage(), e);
    	return ResponseEntity.badRequest().body(ErrorMessage.newInstance(e.getMessage()));
    }

    
    @NoArgsConstructor
    @AllArgsConstructor(staticName="newInstance")
    public static final class ErrorMessage{
    	@Getter private String message;
    }
}