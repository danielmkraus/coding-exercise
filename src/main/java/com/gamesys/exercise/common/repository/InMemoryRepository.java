package com.gamesys.exercise.common.repository;

import static java.util.Optional.ofNullable;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import lombok.Getter;

public abstract class InMemoryRepository<E, ID> implements Wipeable {

	@Getter private Map<ID, E> registeredItems;
	
	public InMemoryRepository() {
		registeredItems = new ConcurrentHashMap<ID, E>();
	}
	
	protected abstract ID getId(final E entity);
	protected abstract void generateId(E entity);

	public void save(final E entity) {
		ID id = ofNullable(getId(entity)).orElseGet(()->generateAndGetId(entity));
		registeredItems.put(id, entity);
	}
	
	public E findById(final ID id) {
		return registeredItems.get(id);
	}
	
	private ID generateAndGetId(E entity){
		generateId(entity);
		return getId(entity);
	}

	@Override
	public void wipe() {
		registeredItems.clear();
	}
	
}
