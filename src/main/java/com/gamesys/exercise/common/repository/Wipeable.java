package com.gamesys.exercise.common.repository;

/**
 * utility to clear storage
 * (used in integration / service tests)
 */
public interface Wipeable {

	/**
	 * wipe all data stored in class.
	 */
	void wipe();
}
