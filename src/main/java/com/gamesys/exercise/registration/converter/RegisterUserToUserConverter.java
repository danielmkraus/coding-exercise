package com.gamesys.exercise.registration.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.gamesys.exercise.registration.domain.User;
import com.gamesys.exercise.registration.dto.RegisterUser;

@Component
public class RegisterUserToUserConverter implements Converter<RegisterUser, User>{

	public User convert(RegisterUser source) {
		return User.builder()
				.dateOfBirth(source.getDateOfBirth())
				.password(source.getPassword())
				.ssn(source.getSsn())
				.username(source.getUsername())
					.build();
	}
	
}
