package com.gamesys.exercise.registration.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gamesys.exercise.registration.domain.User;
import com.gamesys.exercise.registration.dto.RegisterUser;
import com.gamesys.exercise.registration.service.UserService;

@RestController
public class UserController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	UserService service;
	
	@Autowired
	ConversionService conversionService;

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public void register(@RequestBody RegisterUser newUser) {
		LOGGER.debug(new StringBuilder("Requesting /register with ").append( newUser).toString() );
		User user = conversionService.convert(newUser, User.class);
		service.register(user);
		LOGGER.debug("Request /register finished");
	}
}