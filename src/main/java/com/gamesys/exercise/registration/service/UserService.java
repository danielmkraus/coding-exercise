package com.gamesys.exercise.registration.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gamesys.exercise.registration.domain.User;
import com.gamesys.exercise.registration.repository.UserRepository;
import com.gamesys.exercise.registration.validation.UserValidator;

@Service
public class UserService {
	
	private UserRepository repository;
	private UserValidator validator;
	
	public UserService(
			@Autowired UserRepository repository, 
			@Autowired UserValidator validator) {
		this.repository = repository;
		this.validator = validator;
	}

	/**
	 * Validate and insert a new user in database.
	 * 
	 * @param user user to be inserted
	 * @throws IllegalArgumentException if have some validation constraint failed on user 
	 */
	public void register(final User user) {
		validator.validate(user);
		repository.save(user);
	}
}