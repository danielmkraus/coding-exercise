package com.gamesys.exercise.registration.validation;

import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.gamesys.exercise.common.validation.Validation;
import com.gamesys.exercise.common.validation.ValidationResult;
import com.gamesys.exercise.registration.domain.User;


/**
 * Apply bean validation on {@link User} class
 */
@Component
public class UserBeanValidator implements Validation<User> {
	
	private Validator validator;
	
	public UserBeanValidator(@Autowired Validator validator) {
		this.validator = validator;
	}
	
	@Override
	public ValidationResult apply(final User user) {
		String constraintViolationTexts = 
				validator.validate(user).stream()
					.map(ConstraintViolation::getMessage)
					.collect(Collectors.joining("\n"));
		
		return StringUtils.isEmpty(constraintViolationTexts) ? 
				ValidationResult.valid() : 
					ValidationResult.invalid(constraintViolationTexts);
	}
}