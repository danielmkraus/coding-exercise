package com.gamesys.exercise.registration.validation;

import static java.util.Objects.isNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gamesys.exercise.common.validation.Validation;
import com.gamesys.exercise.common.validation.ValidationResult;
import com.gamesys.exercise.exclusion.service.ExclusionService;
import com.gamesys.exercise.registration.domain.User;


/**
 * Validate user against the {@link ExclusionService} to verify if the SSN is blacklisted
 *
 */
@Component
public class UserNotExcludedValidator implements Validation<User>{

	private static final String USER_SSN_BLACKLISTED_MESSAGE_KEY = "User.ssn.blacklisted";
	private ExclusionService exclusionService;
	
	public UserNotExcludedValidator(@Autowired ExclusionService exclusionService) {
		this.exclusionService = exclusionService;
	}
	
	private boolean isValid(final User user) {
		return isNull(user) ||
				!user.getDateOfBirthFormatted().isPresent() ||
				isNull(user.getSsn()) ||
				exclusionService.validate(user.getDateOfBirthFormatted().get(), user.getSsn());
	}

	@Override
	public ValidationResult apply(final User value) {
		return isValid(value) ? 
				ValidationResult.valid() : 
					ValidationResult.invalid(VALIDATION_MESSAGES.getString(USER_SSN_BLACKLISTED_MESSAGE_KEY));
	}
}
