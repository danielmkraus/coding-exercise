package com.gamesys.exercise.registration.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gamesys.exercise.common.validation.ValidationResult;
import com.gamesys.exercise.registration.domain.User;

/**
 * Handle all {@link User} validation.
 */
@Component
public class UserValidator {

	private UserBeanValidator beanValidator;
	private UsernameUniqueValidator usernameUniqueValidator;
	private UserNotExcludedValidator userNotExcludedValidator;
	
	public UserValidator(
			@Autowired UserBeanValidator beanValidator, 
			@Autowired UsernameUniqueValidator usernameUniqueValidator, 
			@Autowired UserNotExcludedValidator userNotExcludedValidator) {
		this.beanValidator = beanValidator;
		this.usernameUniqueValidator = usernameUniqueValidator;
		this.userNotExcludedValidator = userNotExcludedValidator;
	}
	
	public void validate(User user) {
		ValidationResult result = 
				beanValidator
					.and(userNotExcludedValidator)
					.and(usernameUniqueValidator)
						.apply(user);
		
		if(result.isNotValid()) {
			throw new IllegalArgumentException(result.getReason().get());
		}
	}
}
