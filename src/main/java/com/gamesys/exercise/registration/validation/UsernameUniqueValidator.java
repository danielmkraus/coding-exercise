package com.gamesys.exercise.registration.validation;

import static java.util.Objects.isNull;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gamesys.exercise.common.validation.Validation;
import com.gamesys.exercise.common.validation.ValidationResult;
import com.gamesys.exercise.registration.domain.User;
import com.gamesys.exercise.registration.repository.UserRepository;

/**
 * username unique constraint validator class
 * 
 * verify if has a {@link User} with username already registered
 * 
 * if user is null or username is not filled, then validate (don`t applied here, in this case, validate with {@link NotNull} constraint)
 *
 */
@Component
public class UsernameUniqueValidator implements Validation<User>{

	private static final String USER_ALREADY_REGISTERED_MESSAGE_KEY = "User.already.registered";
	
	private UserRepository repository;

	public UsernameUniqueValidator(@Autowired UserRepository userRepository) {
		repository = userRepository;
	}
	
	private boolean isValid(final User user) {
		if(isNull(user) || isNull(user.getUsername())) {
			return true;
		}
		Optional<User> userFound = repository.findByUsername(user.getUsername());
		return !userFound.isPresent() || userFound.get().equals(user);
	}

	@Override
	public ValidationResult apply(final User user) {
		return isValid(user) ? ValidationResult.valid() : ValidationResult.invalid(VALIDATION_MESSAGES.getString(USER_ALREADY_REGISTERED_MESSAGE_KEY));
	}

}
