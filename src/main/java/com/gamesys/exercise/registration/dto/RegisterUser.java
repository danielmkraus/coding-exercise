package com.gamesys.exercise.registration.dto;

import java.time.LocalDate;

import lombok.Data;

@Data
public class RegisterUser {
	private String username;
	private String password;
	private String ssn;
	private LocalDate dateOfBirth;
}
