package com.gamesys.exercise.registration.domain;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(of = "id")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {

	/**
	 * alphanumerical regular expression
	 */
	private static final String REGULAR_EXPRESSION_NAME = "^[\\da-zA-Z]+$";

	/**
	 * <p>
	 * regular expression to match following rules:
	 * </p>
	 * <ul>
	 * <li>at least four characters</li>
	 * <li>at least one upper case character</li>
	 * <li>at least one number</li>
	 * </ul>
	 * 
	 */
	private static final String REGULAR_EXPRESSION_PASSWORD = "^(?=.*[A-Z])(?=.*\\d).{4,}$";

	private String id;

	@Pattern(message = "{User.username.invalid}", regexp = REGULAR_EXPRESSION_NAME)
	@NotNull(message = "{User.username.null}")
	private String username;

	@Pattern(message = "{User.password.invalid}", regexp = REGULAR_EXPRESSION_PASSWORD)
	@NotNull(message = "{User.password.null}")
	private String password;

	@NotNull(message = "{User.dateOfBirth.null}")
	private LocalDate dateOfBirth;

	@NotNull(message = "{User.ssn.null}")
	private String ssn;

	public Optional<String> getDateOfBirthFormatted() {
		return Optional.ofNullable(getDateOfBirth())
				.map(dateOfBirth -> dateOfBirth.format(DateTimeFormatter.ISO_LOCAL_DATE));
	}
}
