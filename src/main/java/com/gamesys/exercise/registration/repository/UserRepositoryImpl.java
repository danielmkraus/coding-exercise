package com.gamesys.exercise.registration.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.gamesys.exercise.common.repository.InMemoryRepository;
import com.gamesys.exercise.registration.domain.User;

@Repository
public class UserRepositoryImpl extends InMemoryRepository<User, String> implements UserRepository {
	
	public Optional<User> findByUsername(String name) {
		return getRegisteredItems().values().parallelStream()
				.filter( user -> user.getUsername().equals(name))
				.findFirst();
	}

	@Override
	public List<User> findBySsnAndDateOfBirth(String ssn, LocalDate dateOfBirth) {
		return  getRegisteredItems().values().parallelStream()
				.filter( user -> Objects.equals(user.getDateOfBirth(), dateOfBirth) && Objects.equals(user.getSsn(),ssn))
				.collect(Collectors.toList());
	}
	
	@Override
	protected String getId(User entity) {
		return entity.getId();
	}

	@Override
	protected void generateId(User entity) {
		String id = UUID.randomUUID().toString();
		entity.setId(id);
	}
}
