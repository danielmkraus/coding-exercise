package com.gamesys.exercise.registration.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import com.gamesys.exercise.registration.domain.User;

public interface UserRepository {

	void save(User entity);
	Optional<User> findByUsername(String name);
	User findById(String id);
	List<User> findBySsnAndDateOfBirth(String ssn, LocalDate parse);
}
